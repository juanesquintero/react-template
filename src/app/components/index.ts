import Registration from './Registration/Registration';
import Login from './Login/Login';
import Courses from './Courses/Courses';
import CreateCourse from './CreateCourse/CreateCourse';

export { Registration, Login, Courses, CreateCourse };
