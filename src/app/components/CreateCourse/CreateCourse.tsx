import React, { useEffect, useState } from 'react';
import Input from '@src/app/common/Input/Input';
import Button from '@src/app/common/Button/Button';
import AuthorsForm from './components/AutorsForm/AuthorsForm';
import getCourseDuration from '@src/app/helpers/getCourseDuration';
import { mockedCoursesList } from '../Courses/Courses.mock';
import { useNavigate } from 'react-router-dom';
import { IAuthor, ICourse } from '../Courses/Courses.types';
import { useForm } from 'react-hook-form';

const CreateCourse: React.FC = () => {
	const [courseAuthors, setCourseAuthors] = useState([]);

	const addCourseAuthor = (author: IAuthor) => {
		if (!courseAuthors.some((a: IAuthor) => a.id === author.id)) {
			setCourseAuthors([...courseAuthors, author]);
		}
	};

	const removeCourseAuthor = (author: IAuthor) => {
		setCourseAuthors(courseAuthors.filter((a: IAuthor) => a.id !== author.id));
	};

	const navigate = useNavigate();

	// TODO FormMik react forms
	const { register, handleSubmit, watch } = useForm<ICourse>({
		defaultValues: {
			title: '',
			description: '',
			duration: 0,
			creationDate: '',
		},
	});

	const onCreate = (data: ICourse) => {
		mockedCoursesList.push(data);
		navigate('/courses');
	};

	const duration = watch('duration', 15);
	// console.log(duration, typeof duration, watch());

	return (
		<div className='my-4'>
			<h5 className='my-4'>Create Course</h5>
			<form
				className='bg-white rounded px-5 py-3'
				onSubmit={handleSubmit(onCreate)}
			>
				<div className='form-group'>
					<div className='form-inline d-flex justify-content-between'>
						<Input
							name='title'
							label='Title'
							type='text'
							placeholder='Python Course No. 1'
							{...register('title', {
								required: true,
								minLength: 3,
								maxLength: 150,
							})}
						/>

						<div className='d-flex align-items-baseline'>
							<Button
								type='submit'
								text='Create course'
								icon='plus'
								style='outline-dark'
							/>
						</div>
					</div>
					<div className='form-group my-3'>
						<Input
							name='description'
							label='Description'
							type='textarea'
							placeholder='Password'
							{...register('description', {
								required: true,
								minLength: 3,
								maxLength: 700,
							})}
						/>
					</div>
					<div className='form-group d-flex align-items-center'>
						<Input
							label='Duration'
							name='duration'
							type='number'
							{...register('duration', {
								valueAsNumber: true,
								required: true,
								min: 15,
								max: 2400,
							})}
						/>
						<p className='mt-5 ms-3'>{getCourseDuration(duration)}</p>
					</div>
				</div>
				<AuthorsForm
					addCourseAuthor={addCourseAuthor}
					removeCourseAuthor={removeCourseAuthor}
					courseAuthors={courseAuthors}
				/>
			</form>
		</div>
	);
};

export default CreateCourse;
